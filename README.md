![输入图片说明](%E6%B5%B7%E6%8A%A52.jpg)
## 说明

> 基于SpringBoot+MyBatis-plus的电商系统，包括前台商城系统及后台管理系统。

> 如果该项目对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！

> 或者您可以 "follow" 一下，该项目将持续更新，不断完善功能。

> 如有问题或者好的建议可以在 Issues 中提。


 **需要演示地址（S2B2C、B2B2C、B2C、O2O等多模式商城）联系下方客服** 
![输入图片说明](mallplus-wechat/libs/webwxgetmsgimg%20(2).png)



## 项目介绍

智汇商城是一款持续更新得轻量级、高性能、前后端分离的 **电商系统** ，包含 小程序 、 APP 、 H5 、 PC 等多终端。我们具有多种商业模式满足你对商城源码得各种需求，其中有  **S2B2C供应链商城** 、 **B2B2C多商户商城** 、 **B2C单商户商城** 、 **O2O外卖商城** 、 **社区团购**  等多种商业模式并且含有 装修模板、分账、用户等级、会员、直播、秒杀、优惠卷、拼团、同城、满减 等一系列特色商城功能，还有更多DIY功能等你开发

### 项目演示（S2B2C、B2B2C、B2C、O2O等多模式商城）联系下方客服
![输入图片说明](mallplus-wechat/libs/webwxgetmsgimg%20(2).png)
### 商城介绍
![输入图片说明](mallplus-wechat/libs/%E5%A4%9A%E4%B8%9A%E5%8A%A1%E6%A8%A1%E5%9D%97%E8%AE%B2%E8%A7%A3.jpg)

### 技术选型

#### 后端技术

技术 | 说明 | 官网
----|----|----
Spring Boot | 容器+MVC框架 | [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
Spring Security | 认证和授权框架 | [https://spring.io/projects/spring-security](https://spring.io/projects/spring-security)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
MyBatisGenerator | 数据层代码生成 | [http://www.mybatis.org/generator/index.html](http://www.mybatis.org/generator/index.html)
PageHelper | MyBatis物理分页插件 | [http://git.oschina.net/free/Mybatis_PageHelper](http://git.oschina.net/free/Mybatis_PageHelper)
Swagger-UI | 文档生产工具 | [https://github.com/swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui)
Hibernator-Validator | 验证框架 | [http://hibernate.org/validator/](http://hibernate.org/validator/)
Elasticsearch | 搜索引擎 | [https://github.com/elastic/elasticsearch](https://github.com/elastic/elasticsearch)
RabbitMq | 消息队列 | [https://www.rabbitmq.com/](https://www.rabbitmq.com/)
Redis | 分布式缓存 | [https://redis.io/](https://redis.io/)
MongoDb | NoSql数据库 | [https://www.mongodb.com/](https://www.mongodb.com/)
Docker | 应用容器引擎 | [https://www.docker.com/](https://www.docker.com/)
Druid | 数据库连接池 | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
OSS | 对象存储 | [https://github.com/aliyun/aliyun-oss-java-sdk](https://github.com/aliyun/aliyun-oss-java-sdk)
JWT | JWT登录支持 | [https://github.com/jwtk/jjwt](https://github.com/jwtk/jjwt)
LogStash | 日志收集 | [https://github.com/logstash/logstash-logback-encoder](https://github.com/logstash/logstash-logback-encoder)
Lombok | 简化对象封装工具 | [https://github.com/rzwitserloot/lombok](https://github.com/rzwitserloot/lombok)

#### 前端技术

技术 | 说明 | 官网
----|----|----
Vue | 前端框架 | [https://vuejs.org/](https://vuejs.org/)
Vue-router | 路由框架 | [https://router.vuejs.org/](https://router.vuejs.org/)
Vuex | 全局状态管理框架 | [https://vuex.vuejs.org/](https://vuex.vuejs.org/)
Element | 前端UI框架 | [https://element.eleme.io/](https://element.eleme.io/)
Axios | 前端HTTP框架 | [https://github.com/axios/axios](https://github.com/axios/axios)
v-charts | 基于Echarts的图表框架 | [https://v-charts.js.org/](https://v-charts.js.org/)
Js-cookie | cookie管理工具 | [https://github.com/js-cookie/js-cookie](https://github.com/js-cookie/js-cookie)
nprogress | 进度条控件 | [https://github.com/rstacruz/nprogress](https://github.com/rstacruz/nprogress)


 **- 版权声明** 
- 本项目由智汇商城开发，禁止未经授权用于商业用途。个人学习可免费使用。如需商业授权，请加微信，获取域名授权。

### 我的微信号
![输入图片说明](mallplus-wechat/libs/webwxgetmsgimg%20(2).png)
 
